package com.yelp.api.scrapper.yelpreviewsscrapper;

import com.yelp.api.scrapper.services.ScrapperServiceImpl;
import com.yelp.api.scrapper.vo.AvatarDetails;
import org.junit.Assert;
import org.junit.Test;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class YelpReviewsScrapperApplicationTests {


	@Test
	public void contextLoads() {
	}

	@Test
	public void googleFaceDetect() throws Exception {
		System.out.println("googleFaceDetect:run");
		String getenv = System.getenv("GOOGLE_APPLICATION_CREDENTIALS");
		Assert.assertNotNull(getenv,"GOOGLE_APPLICATION_CREDENTIALS Missing");
		ScrapperServiceImpl scrapperService = new ScrapperServiceImpl();
		String picURL = "https://s3-media1.fl.yelpcdn.com/photo/7xyl-Isqq1qC-apQorwbcw/90s.jpg";

		AvatarDetails avatarDetails  = scrapperService.detectFaceAvatar(picURL);
		System.out.println(avatarDetails);
	}

	@Test
	public void scrapWebsite() throws Exception {
		System.out.println("scrapWebsite:run");
		//https://www.yelp.com/biz/milwaukee-ale-house-milwaukee
		String url = "https://www.yelp.com/biz/milwaukee-ale-house-milwaukee";
		ScrapperServiceImpl scrapperService = new ScrapperServiceImpl();
		scrapperService.scrapWebsite(url);
	}




}
