package com.yelp.api.scrapper.vo;

public class ReviewDetails3 {

    private String username;
    private String userLocation;
    private String usersNumberOfFriends;
    private String usersNumberOfReviews;
    private String usersNumberOfPhotos;
    private String rating;
    private String review;
    private AvatarDetails avatarDetails;

    public ReviewDetails3() {
    }

    public ReviewDetails3(String username, String userLocation, String usersNumberOfFriends, String usersNumberOfReviews, String usersNumberOfPhotos, String rating, String review, AvatarDetails avatarDetails) {
        this.username = username;
        this.userLocation = userLocation;
        this.usersNumberOfFriends = usersNumberOfFriends;
        this.usersNumberOfReviews = usersNumberOfReviews;
        this.usersNumberOfPhotos = usersNumberOfPhotos;
        this.rating = rating;
        this.review = review;
        this.avatarDetails = avatarDetails;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public String getUsersNumberOfFriends() {
        return usersNumberOfFriends;
    }

    public void setUsersNumberOfFriends(String usersNumberOfFriends) {
        this.usersNumberOfFriends = usersNumberOfFriends;
    }

    public String getUsersNumberOfReviews() {
        return usersNumberOfReviews;
    }

    public void setUsersNumberOfReviews(String usersNumberOfReviews) {
        this.usersNumberOfReviews = usersNumberOfReviews;
    }

    public String getUsersNumberOfPhotos() {
        return usersNumberOfPhotos;
    }

    public void setUsersNumberOfPhotos(String usersNumberOfPhotos) {
        this.usersNumberOfPhotos = usersNumberOfPhotos;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public AvatarDetails getAvatarDetails() {
        return avatarDetails;
    }

    public void setAvatarDetails(AvatarDetails avatarDetails) {
        this.avatarDetails = avatarDetails;
    }
}
