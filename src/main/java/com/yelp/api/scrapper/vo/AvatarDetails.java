package com.yelp.api.scrapper.vo;

public class AvatarDetails {

        private String joyLikelihood;
        private String sorrowLikelihood;


        public AvatarDetails() {
        }

        public AvatarDetails(String joyLikelihood, String sorrowLikelihood) {
            this.joyLikelihood = joyLikelihood;
            this.sorrowLikelihood = sorrowLikelihood;
        }

        public String getJoyLikelihood() {
            return joyLikelihood;
        }

        public void setJoyLikelihood(String joyLikelihood) {
            this.joyLikelihood = joyLikelihood;
        }

        public String getSorrowLikelihood() {
            return sorrowLikelihood;
        }

        public void setSorrowLikelihood(String sorrowLikelihood) {
            this.sorrowLikelihood = sorrowLikelihood;
        }

    @Override
    public String toString() {
        return "AvatarDetails{" +
                "joyLikelihood='" + joyLikelihood + '\'' +
                ", sorrowLikelihood='" + sorrowLikelihood + '\'' +
                '}';
    }
}
