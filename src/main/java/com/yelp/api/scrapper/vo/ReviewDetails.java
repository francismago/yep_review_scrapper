package com.yelp.api.scrapper.vo;

public class ReviewDetails {

    private String username;
    private String userLocation;
    private String rating;
    private String date;
    private String review;
    private AvatarDetails avatarDetails;

    public ReviewDetails() {
    }

    public ReviewDetails(String username, String userLocation, String rating, String date, String review, AvatarDetails avatarDetails) {
        this.username = username;
        this.userLocation = userLocation;
        this.rating = rating;
        this.date = date;
        this.review = review;
        this.avatarDetails = avatarDetails;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public AvatarDetails getAvatarDetails() {
        return avatarDetails;
    }

    public void setAvatarDetails(AvatarDetails avatarDetails) {
        this.avatarDetails = avatarDetails;
    }

    @Override
    public String toString() {
        return "ReviewDetails{" +
                "username='" + username + '\'' +
                ", userLocation='" + userLocation + '\'' +
                ", rating='" + rating + '\'' +
                ", review='" + review + '\'' +
                ", avatarDetails=" + avatarDetails +
                '}';
    }

    public static class  Builder{
        private String username;
        private String userLocation;
        private String rating;
        private String date;
        private String review;
        private AvatarDetails avatarDetails;

        public Builder username(String username){
            this.username = username;
            return this;
        }

        public Builder userLocation(String userLocation){
            this.userLocation = userLocation;
            return this;
        }

        public Builder rating(String rating){
            this.rating = rating;
            return this;
        }


        public Builder date(String date){
            this.date = date;
            return this;
        }

        public Builder review(String review){
            this.review = review;
            return this;
        }

        public Builder avatarDetails(AvatarDetails avatarDetails){
            this.avatarDetails = avatarDetails;
            return this;
        }


        public ReviewDetails createReviewDetails(){
            return new ReviewDetails(username,userLocation,rating,date,review,avatarDetails);
        }





    }

}
