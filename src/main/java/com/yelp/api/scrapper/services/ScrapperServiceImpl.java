package com.yelp.api.scrapper.services;

import com.google.cloud.vision.v1.*;
import com.google.protobuf.ByteString;
import com.yelp.api.scrapper.interfaces.ScrapperService;
import com.yelp.api.scrapper.vo.AvatarDetails;
import com.yelp.api.scrapper.vo.ReviewDetails;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service("scrapperService")
public class ScrapperServiceImpl implements ScrapperService {

    private static Logger logger = LoggerFactory.getLogger(ScrapperServiceImpl.class);

    @Override
    public List<ReviewDetails> getReviews(String url) {
        logger.info("getReviews:url"+url);

        List<ReviewDetails> reviews = new ArrayList<>();
        try {
            reviews =  scrapWebsite(url);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return reviews;
    }




    public List<ReviewDetails> scrapWebsite(String url) throws Exception {
        Document doc = Jsoup.connect(url).get();
        //reviews ->? ul
        Elements reviewsUL = doc.select("ul.reviews");
        Elements reviews  = reviewsUL.select("div.review");

        List<ReviewDetails> reviewDetails = new ArrayList<>();
        int ctr = 0;
        for (Element e : reviews){
            if (ctr++ == 0)
                continue;
            String username = e.getElementsByClass("user-display-name").text();
            String userLocation =  e.getElementsByClass("user-location").text();
            String rating = e.getElementsByClass("i-stars").attr("title").toString();
            String date = e.getElementsByClass("rating-qualifier").text();
            String review = e.getElementsByTag("p").get(0).text();
            String avatarLink = e.getElementsByClass("photo-box-img").attr("src").toString();

            AvatarDetails avatarDetails = detectFaceAvatar(avatarLink);

            ReviewDetails   details = new ReviewDetails.Builder()
                    .username(username)
                    .userLocation(userLocation)
                    .rating(rating)
                    .date(date)
                    .review(review)
                    .avatarDetails(avatarDetails != null ? avatarDetails : null)
                    .createReviewDetails();
            reviewDetails.add(details);
        }
        return reviewDetails;

    }


    public AvatarDetails detectFaceAvatar(String imageURL) throws Exception, IOException {
        List<AnnotateImageRequest> requests = new ArrayList<>();


        URL url = new URL(imageURL);
        InputStream imageStream = url.openStream();
        ByteString imgBytes = ByteString.readFrom(imageStream);

        Image img = Image.newBuilder().setContent(imgBytes).build();
        Feature feat = Feature.newBuilder().setType(Feature.Type.FACE_DETECTION).build();
        AnnotateImageRequest request =
                AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img).build();
        requests.add(request);


        try (ImageAnnotatorClient client = ImageAnnotatorClient.create()) {
            BatchAnnotateImagesResponse response = client.batchAnnotateImages(requests);
            List<AnnotateImageResponse> responses = response.getResponsesList();


            for (AnnotateImageResponse res : responses) {
                if (res.hasError()) {
                    System.out.println("Error: " +res.getError().getMessage());
                    return null;
                }
                // For full list of available annotations, see http://g.co/cloud/vision/docs

                if (res.getFaceAnnotationsList().size() != 0 ){
                    FaceAnnotation annotation =  res.getFaceAnnotationsList().get(0);
                    AvatarDetails avatarDetails = new AvatarDetails();
                    avatarDetails.setJoyLikelihood(annotation.getJoyLikelihood().toString());
                    avatarDetails.setSorrowLikelihood(annotation.getSorrowLikelihood().toString());
                    return avatarDetails;
                }
            }
        }
        return null;
    }
}
