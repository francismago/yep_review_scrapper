package com.yelp.api.scrapper.interfaces;

import com.yelp.api.scrapper.vo.ReviewDetails;

import java.util.List;

public interface ScrapperService {

    public List<ReviewDetails> getReviews(String url);
}
