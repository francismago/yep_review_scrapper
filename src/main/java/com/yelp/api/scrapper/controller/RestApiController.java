package com.yelp.api.scrapper.controller;

import com.google.gson.Gson;
import com.yelp.api.scrapper.interfaces.ScrapperService;
import com.yelp.api.scrapper.vo.ReviewDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RestApiController {

    private static Logger logger = LoggerFactory.getLogger(RestController.class);

    @Autowired
    private ScrapperService scrapperService;


    @RequestMapping(value = "/reviews", method = RequestMethod.GET)
    public String getReviews(@RequestParam String url){
        List<ReviewDetails>  reviews = scrapperService.getReviews(url);
        logger.info("reviews="+reviews);
        Gson gson = new Gson();
        String response = gson.toJson(reviews);
        logger.info("response="+response    );
        return response ;
    }

}
