package com.yelp.api.scrapper.yelpreviewsscrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.yelp.api.scrapper"})
public class YelpReviewsScrapperApplication {

	public static void main(String[] args) {
		SpringApplication.run(YelpReviewsScrapperApplication.class, args);
	}
}
