Configuration Info:

1. Create an environment variable for Google Cloud API credential,
you may get it in gcloud folder


Variable = GOOGLE_APPLICATION_CREDENTIALS
Value = [path file]/avatar-link-test-bdb6b050c45c.json

2. Run this project with maven Spring boot


3. Access API

API Path:
[server]//api/reviews?url=[link]

Param:
url = yelp link that you want scrap the reviews

Example :
http://localhost:8080/api/reviews?url=https://www.yelp.com/biz/milwaukee-ale-house-milwaukee

